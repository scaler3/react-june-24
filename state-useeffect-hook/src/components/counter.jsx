import { useEffect, useState } from "react";

const Counter = ({ count, setCount }) => {

  useEffect(() => {
    console.log(`Counter Component is Mounted`);

    return () => {
        console.log(`Counter Component Unmounted`)
    }
  }, [])

  useEffect(() => {
    console.log(`Update Count`, count);
    
    return () => {
        console.log(`Unmount Update Count`, count)
    }
    

}, [count])



  return (
    <div>
      <button onClick={() => setCount(count - 1)}>-</button>
      <span>{count}</span>
      <button onClick={() => setCount(count + 1)}>+</button>
    </div>
  );
};

export default Counter;
