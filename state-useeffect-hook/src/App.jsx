import Counter from "./components/counter"
import { useState } from "react";

function App() {
  const [count, setCount] = useState(0);


  return (
    <>

      <Counter count={count} setCount={setCount} />
      <Counter count={count} setCount={setCount} />
      <Counter count={count} setCount={setCount} />
      <Counter count={count} setCount={setCount} />
    </>
  )
}

export default App
