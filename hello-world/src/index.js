import React, { useState } from "react";
import ReactDOM from "react-dom/client";

function MyCounter({ value, changeCount }) {
  // Props

  return (
    <div>
      <button onClick={(e) => changeCount(value - 1)}>Decrement</button>
      <h3>{value}</h3>
      <button onClick={(e) => changeCount(value + 1)}>Increment</button>
    </div>
  );
}

function MyComponent() {
  const [value, setValue] = useState(1);


  console.log('Component is re-rendering', value)

  return (
    <div>
      <MyCounter value={value} changeCount={setValue}></MyCounter>

      <hr />

      <MyCounter value={value} changeCount={setValue}></MyCounter>

      <MyCounter value={value} changeCount={setValue}></MyCounter>

      <MyCounter value={value} changeCount={setValue}></MyCounter>

      <MyCounter value={value} changeCount={setValue}></MyCounter>
    </div>
  );
}

const root = ReactDOM.createRoot(document.getElementById("app"));
root.render(<MyComponent></MyComponent>);
