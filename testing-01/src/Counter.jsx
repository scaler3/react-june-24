import React, { useState } from "react";

const Counter = ({ onIncrement }) => {
  const [count, setCount] = useState(1);

  const handleIncrement = () => {
    if (onIncrement) onIncrement();
    setCount(count + 1);
  };

  return (
    <div>
      <button onClick={() => (count !== 0 ? setCount(count - 1) : null)}>
        -
      </button>
      <h6>Count is {count}</h6>
      <button onClick={handleIncrement}>+</button>
    </div>
  );
};

export default Counter;
