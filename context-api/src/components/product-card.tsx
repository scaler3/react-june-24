import { Button } from "../@/components/ui/button";
import { Item, useCart } from "@/context/cart";
import React from "react";

interface ProductCardProps {
  item: Item;
}

const ProductCard: React.FC<ProductCardProps> = ({ item }) => {
  const context = useCart();

  return (
    <div className="p-5 border-dashed border-2 w-fit rounded-lg">
      <h3>{item.title}</h3>
      <h6>INR {item.price}</h6>
      <Button onClick={() => context?.addToCart(item)}>Add To Cart</Button>
    </div>
  );
};

export default ProductCard;
