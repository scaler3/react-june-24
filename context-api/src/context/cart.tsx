import React, { useContext, useState } from "react";

export interface Item {
  id: string;
  title: string;
  price: number;
}

interface ICartContext {
  items: Item[];
  addToCart: (item: Item) => void;
}

const CartContext = React.createContext<ICartContext | null>(null);

export const useCart = () => {
  const state = useContext(CartContext);
  return state;
};

export const CartProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [items, setItems] = useState<Item[]>([]);

  return (
    <CartContext.Provider
      value={{ items, addToCart: (item) => setItems([...items, item]) }}
    >
      {children}
    </CartContext.Provider>
  );
};
