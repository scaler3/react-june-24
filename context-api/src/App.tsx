import "./App.css";
import ProductCard from "./components/product-card";
import { useCart } from "./context/cart";

function App() {
  const context = useCart();

  return (
    <div className="p-5 grid grid-flow-col">
      <div>
        <ProductCard item={{ id: "1", price: 20, title: "Shoes" }} />
        <ProductCard item={{ id: "1", price: 20, title: "Keyboard" }} />
        <ProductCard item={{ id: "1", price: 20, title: "Mouse" }} />
        <ProductCard item={{ id: "1", price: 20, title: "Laptop" }} />
      </div>
      <div>
        <h1>Cart ({context?.items.length})</h1>
        {context?.items.map((item) => (
          <ProductCard key={item.id} item={item} />
        ))}
      </div>
    </div>
  );
}

export default App;
