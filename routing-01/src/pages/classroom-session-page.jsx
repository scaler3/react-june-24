import { useParams } from "react-router-dom"

const ClassRoomPage = () => {
    const {classID, sessionId} = useParams()
    

    return (
        <div>
            <h1>Class Room Page for Class {classID} and Session {sessionId}</h1>
        </div>
    )
}

export default ClassRoomPage