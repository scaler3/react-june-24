import { Routes, Route, Link, Navigate } from "react-router-dom";
import "./App.css";

// Pages
import HomePage from "./pages/home-page";
import ContactPage from "./pages/contact-page";
import ClassRoomPage from "./pages/classroom-session-page";

function App() {
  return (
    <div>

      <nav style={{ display: "flex", gap: "10px" }}>
        <Link to="/">Home</Link>
        <Link to="/contact">Contact</Link>
        <Link to="/mentee/class/1/session/1">Join Class</Link>
      </nav>

      <Routes>

        <Route path="/" element={<HomePage />} />
        <Route path="/contact" element={<ContactPage />} />
        <Route
          path="/mentee/class/:classID/session/:sessionId"
          element={<ClassRoomPage />}
        />
    <Route path="/home" element={<Navigate to="/" />} />
      </Routes>
    </div>
  );
}

export default App;
