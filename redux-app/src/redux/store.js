import { configureStore } from "@reduxjs/toolkit";

import counterReducer from "./slices/counterSlice";
import todoReducer from "./slices/todoSlice";

// This creates redux store for us and exports store variable
export const store = configureStore({
  reducer: {
    counter: counterReducer,
    todo: todoReducer,
  },
  devTools: true,
});
